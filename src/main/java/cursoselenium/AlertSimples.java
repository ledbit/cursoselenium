package cursoselenium;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.junit.Assert;

public class AlertSimples {

	CampoDeTreinamentoTest campo = new CampoDeTreinamentoTest();
	@Test
	public void deveInteragirComAlertSimples() {
		WebDriver driver = campo.getDriver();
		driver.findElement(By.id("alert")).click();
		
		Alert alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Alert Simples", texto);
		
		alert.accept();
		driver.findElement(By.id("elementosForm:nome")).sendKeys(texto);
		driver.quit();
	}
	
	@Test
	public void deveInteragirComAlertConfirm() {
		WebDriver driver = campo.getDriver();
		driver.findElement(By.id("confirm")).click();
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		String texto = alert.getText();
		Assert.assertEquals("Confirmado", texto);
		alert.accept();
		
		//driver.switchTo().defaultContent();
		driver.findElement(By.id("confirm")).click();
		
		alert = driver.switchTo().alert();
		alert.dismiss();
		texto = alert.getText();
		Assert.assertEquals("Negado", texto);
		
		driver.quit();
	}
	@Test
	public void deveInteragirComAlertPrompt() {
		WebDriver driver = campo.getDriver();
		driver.findElement(By.id("prompt")).click();
		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("Digite um numero", alert.getText());
		alert.sendKeys("273");
		alert.accept();
		Assert.assertEquals("Era 273?", alert.getText());
		alert.accept();
		Assert.assertEquals(":D", alert.getText());
		alert.accept();
		
		driver.quit();
	}
}
