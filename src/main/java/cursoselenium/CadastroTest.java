package cursoselenium;


import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CadastroTest {

	CampoDeTreinamentoTest campo = new CampoDeTreinamentoTest();
	public WebDriver getDriver() {
		WebDriver driver = campo.getDriver();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html");
		return driver;
	}
	
	@Test
	public void deveCadastrarUsuario() {
		WebDriver driver = getDriver();
		
		//inserindo dados
		driver.findElement(By.id("elementosForm:nome")).sendKeys("Maicrossofiti");
		driver.findElement(By.id("elementosForm:sobrenome")).sendKeys("Ferracini");
		driver.findElement(By.id("elementosForm:sexo:0")).click();
		driver.findElement(By.id("elementosForm:comidaFavorita:2")).click();
		
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select select = new Select(element);
		select.selectByVisibleText("Mestrado");		
		
		new Select(driver.findElement(By.id("elementosForm:esportes"))).selectByVisibleText("Natacao");
		
		//aciona bot�o cadastrar
		driver.findElement(By.id("elementosForm:cadastrar")).click();
		
		//validar dados cadastrados
//		List<WebElement> elementos = driver.findElements(By.id("resultado"));
		//Assert.assertEquals("Cadastrado!", driver.findElement(By.id("resultado")).getText());
		Assert.assertEquals("Nome: Maicrossofiti", driver.findElement(By.id("descNome")).getText());
		Assert.assertEquals("Sobrenome: Ferracini", driver.findElement(By.id("descSobrenome")).getText());
		Assert.assertEquals("Sexo: Masculino", driver.findElement(By.id("descSexo")).getText());
		Assert.assertEquals("Comida: Pizza", driver.findElement(By.id("descComida")).getText());
		Assert.assertEquals("Escolaridade: mestrado", driver.findElement(By.id("descEscolaridade")).getText());
		Assert.assertEquals("Esportes: Natacao", driver.findElement(By.id("descEsportes")).getText());
		//Assert.assertEquals("Maicrossofiti", driver.findElement(By.id("descSugestoes")).getText());
		
		
		driver.quit();
	}
	
	
}
