package cursoselenium;

import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CampoDeTreinamentoTest {

	public WebDriver getDriver() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file:///" + System.getProperty("user.dir") + "/src/test/resources/componentes.html");
		
		driver.manage().window().maximize();
		return driver;
	}

	@Test
	public void verificaTitulo() {
		// TODO Auto-generated method stub
		WebDriver driver = getDriver();
		Assert.assertEquals("Campo de Treinamento", driver.getTitle());
		// driver.close();//kill only the driver
		driver.quit();// kill the driver and the browser
	}

	@Test
	public void deveInteragirComTextfield() {

		WebDriver driver = getDriver();
		driver.findElement(By.id("elementosForm:nome")).sendKeys("Say my name");
		Assert.assertEquals("Say my name", driver.findElement(By.id("elementosForm:nome")).getAttribute("value"));

		driver.quit();
	}

	@Test
	public void deveInteragirComTextarea() {
		WebDriver driver = getDriver();
		driver.findElement(By.id("elementosForm:sugestoes")).sendKeys("Linha 1\nLinha2\nLinha3\n.\n.\n.\nLinha n");

		Assert.assertEquals("Linha 1\nLinha2\nLinha3\n.\n.\n.\nLinha n", driver.findElement(By.id("elementosForm:sugestoes")).getAttribute("value"));

		driver.quit();
	}

	@Test
	public void deveInteragirComRadioButton() {
		WebDriver driver = getDriver();
		driver.findElement(By.id("elementosForm:sexo:0")).click();
		Assert.assertTrue(driver.findElement(By.id("elementosForm:sexo:0")).isSelected());
		driver.quit();
	}
	@Test
	public void deveInteragirComCheckbox() {
		WebDriver driver = getDriver();
		driver.findElement(By.id("elementosForm:comidaFavorita:2")).click();
		Assert.assertTrue(driver.findElement(By.id("elementosForm:comidaFavorita:2")).isSelected());
		driver.quit();
	}
	@Test
	public void deveInteragirComCombobox() {
		WebDriver driver = getDriver();
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select select = new Select(element);
		select.selectByVisibleText("2o grau completo");
		Assert.assertEquals("2o grau completo", select.getFirstSelectedOption().getText());
		driver.quit();
	}
	@Test
	public void deveVerificarValoresCombobox() {
		WebDriver driver = getDriver();
		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select select = new Select(element);
		List<WebElement> options = select.getOptions();		
		Assert.assertEquals(8, options.size());
		boolean encontrou = false;
		for (WebElement option : options) {
			if(option.getText().equals("Mestrado")) {
				encontrou = true;
				break;
			}
		}
		Assert.assertTrue(encontrou);
		driver.quit();
	}
	@Test
	public void deveInteragirComComboboxMultiplaEscolha() {
		WebDriver driver = getDriver();
		WebElement element = driver.findElement(By.id("elementosForm:esportes"));
		Select select = new Select(element);
		select.selectByVisibleText("Natacao");
		select.selectByVisibleText("Karate");
		select.selectByVisibleText("Corrida");
		List<WebElement> lista = select.getAllSelectedOptions();
		Assert.assertEquals(3, lista.size());
		
		select.deselectByVisibleText("Corrida");
		lista = select.getAllSelectedOptions();
		Assert.assertEquals(2, lista.size());
		driver.quit();
	}
	
	@Test
	public void deveInteragirComBotoes() {
		WebDriver driver = getDriver();
		WebElement element = driver.findElement(By.id("buttonSimple"));
		element.click();
		Assert.assertEquals("Obrigado!", element.getAttribute("value"));
		driver.quit();
	}
	@Test
	public void deveInteragirLinks() {
		WebDriver driver = getDriver();
		WebElement element = driver.findElement(By.linkText("Voltar"));
		element.click();
		
		Assert.assertEquals("Voltou!", driver.findElement(By.id("resultado")).getText());
		driver.quit();
	}
	@Test
	public void deveBuscarTextoNaPagina() {
		WebDriver driver = getDriver();
		WebElement element = driver.findElement(By.tagName("h3"));
		
//		Assert.assertTrue(driver.findElement(By.tagName("body")).getText().contains("Campo de Treinamento"));
		
		Assert.assertEquals("Campo de Treinamento", element.getText());
		
		Assert.assertEquals("Cuidado onde clica, muitas armadilhas...", driver.findElement(By.className("facilAchar")).getText());
		driver.quit();
	}
	
	
}
