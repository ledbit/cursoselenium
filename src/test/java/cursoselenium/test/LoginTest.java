package cursoselenium.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ferracini.pattern.LoginPage;

public class LoginTest {

	WebDriver driver;
	@BeforeTest
	public void setup() {
		System.getProperty("webdriver.firefox.marionette", "pathToGeckodriver");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("");
	}
	@Test(priority=5)
	public void verify1()
	{
	    LoginPage login=new LoginPage(driver);
	    login.set_username("admin");
	    login.set_password("admin");
	    login.click_button();
	    Assert.assertTrue(driver.getPageSource().contains("Blog"));
	}
	@Test(priority=1)
	public void verify2()
	{
	    LoginPage login=new LoginPage(driver);
	    login.set_username("adm");
	    login.set_password("admin");
	    login.click_button();
	    Assert.assertEquals(driver.findElement(By.xpath("//*[@id='errorMessage']")),"Wrong username or password!");
	}
	@Test(priority=2)
	public void verify3()
	{
	    LoginPage login=new LoginPage(driver);
	    login.set_username("admin");
	    login.set_password("adm");
	    login.click_button();
	    Assert.assertEquals(driver.findElement(By.xpath("//*[@id='errorMessage']")),"Wrong username or password!");
	}
}
